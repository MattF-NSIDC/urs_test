FROM continuumio/miniconda3:latest

COPY environment.yml /environment.yml
RUN conda env create -f environment.yml

RUN mkdir /app
WORKDIR /app
ENV PATH /opt/conda/envs/urs_test/bin:$PATH
