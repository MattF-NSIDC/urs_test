from base64 import b64encode
import os
from pprint import pprint

import requests


url='https://n5eil10u.ecs.nsidc.org/opendap/hyrax/OPS/user/DP5/MOST/MOD10A1.006/2007.11.10/MOD10A1.A2007314.h14v03.006.2016097121609.hdf.nc?NDSI_Snow_Cover'
creds = (os.environ['EARTHDATA_USER'], os.environ['EARTHDATA_PASS'])
creds_string = creds[0] + ':' + creds[1]
b64_creds = b64encode(creds_string.encode('utf-8')).decode('utf-8')

s = requests.Session()

attempt1_response = s.get(url, verify=False, allow_redirects=False)
attempt2_url = attempt1_response.headers['location']
attempt2_response = s.get(attempt2_url, verify=False,
                          headers={'Authorization': 'Basic: ' + b64_creds})

print('Data URL: {}'.format(url))
print('Redirected to URS URL: {}'.format(attempt2_url))
print('History of URS request:')
pprint([r.url for r in attempt2_response.history], indent=4)
print('Redirected to data URL: {}'.format(attempt2_response.url))
