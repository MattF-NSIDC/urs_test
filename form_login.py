import os

from bs4 import BeautifulSoup
import requests
from six.moves.urllib.parse import urlsplit, urlunsplit

s = requests.Session()
jar = requests.cookies.RequestsCookieJar()

creds = {'username': os.environ['EARTHDATA_USER'], 'password': os.environ['EARTHDATA_PASS']}

auth_url = 'https://urs.earthdata.nasa.gov'
data_url = 'https://n5eil01u.ecs.nsidc.org/ICEBRIDGE/IODMS1B.001/2009.12.08/DMS_1000133_00333_20091208_22240330_V02.tif'

def _get_action_url(current_url, relative_action_url):
    split_current = urlsplit(current_url)
    split_action = urlsplit(relative_action_url)
    comb = [val2 if val1 == '' else val1
            for val1, val2 in zip(split_action, split_current)]
    return urlunsplit(comb)


def bs4_urs_login(session=s, url=auth_url, creds=creds):
    resp = session.get(url)

    soup = BeautifulSoup(resp.content, 'lxml')
    login_form = soup.select('form')[0]
    action_url = _get_action_url(resp.url, login_form.get('action'))

    payload = {}
    for inp in login_form.findAll('input'):
        if (inp.get('name') not in payload and
            inp.get('name') is not None):
            fieldname = inp.get('name')
            if fieldname in ['username', 'password']:
                fieldval = creds[fieldname]
            else:
                fieldval = inp.get('value')
            payload.update({fieldname: fieldval})
    
    return session.post(action_url, data=payload)


def write_data(data_url=data_url):

    # Authenticate with URS with the slightest hint it might be needed
    if data_url.startswith('https://'):
        auth_resp = bs4_urs_login()

    # Get the data
    data_resp = s.get(data_url, verify=False)
    filename = 'formlogin_data-' + data_url.split('/')[-1]
    with open(os.path.join('data', filename), 'wb') as f:
        f.write(data_resp.content)


if __name__ == '__main__':
    write_data()
